package princes.princes.comercio.electronico.servicio;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;

import princes.princes.comercio.electronico.entidad.Princes;

@Repository
@Transactional
public class PrincesDAOServicio {

	@PersistenceContext
	private EntityManager entityManager;
	
	public long insert(Princes producto){
		entityManager.persist(producto);
		return producto.getBrandId();
	}
	
}
