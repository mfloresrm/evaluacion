package princes.princes.comercio.electronico;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import princes.princes.comercio.electronico.entidad.Princes;
import princes.princes.comercio.electronico.servicio.PrincesDAOServicio;


@Component
public class PrincesDaoCommandLine implements CommandLineRunner {

	private static final Logger log =
			LoggerFactory.getLogger(PrincesDaoCommandLine.class);
	
	@Autowired
	private PrincesDAOServicio princesDaoService;
	
	@Override
	public void run(String... args) throws Exception {
		
		//Insercion de datos en H2
		Princes prince = new Princes(1, "2020-06-14-00.00.00", "2020-12-31-23.59.59", 1, 35455, 0, 35.50, "EUR");
		Princes prince2 = new Princes(1, "2020-06-14-15.00.00", "2020-06-14-18.30.00", 2, 35455, 1, 25.45, "EUR");
		Princes prince3 = new Princes(1, "2020-06-15-00.00.00", "2020-06-15-11.00.00", 3, 35455, 1, 30.50, "EUR");
		Princes prince4 = new Princes(1, "2020-06-15-16.00.00", "2020-12-31-23.59.59", 4, 35455, 1, 38.95, "EUR");
		long insert = princesDaoService.insert(prince);
		long insert2 = princesDaoService.insert(prince2);
		long insert3 = princesDaoService.insert(prince3);
		long insert4 = princesDaoService.insert(prince4);
		log.info("Nuevo producto creado : " + prince);
		log.info("Nuevo producto creado : " + prince2);
		log.info("Nuevo producto creado : " + prince3);
		log.info("Nuevo producto creado : " + prince4);
	}
	
}
