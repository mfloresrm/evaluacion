package princes.princes.comercio.electronico.entidad;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="Princes")
public class Princes {
	public Princes() {
		super();
	}

	public Princes(Integer brandId, String startDate, String endDate, Integer priceList, Integer productId,
			Integer priority, Double price, String curr) {
		super();
		this.brandId = brandId;
		this.startDate = startDate;
		this.endDate = endDate;
		this.priceList = priceList;
		this.productId = productId;
		this.priority = priority;
		this.price = price;
		this.curr = curr;
	}

	private String startDate;

	private String endDate;

	@Id
	private Integer priceList;

	private Integer productId;

	private Integer priority;

	private Double price;

	private String curr;

	private Integer brandId;

	public Integer getBrandId() {
		return brandId;
	}

	public void setBrandId(Integer brandId) {
		this.brandId = brandId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public Integer getPriceList() {
		return priceList;
	}

	public void setPriceList(Integer priceList) {
		this.priceList = priceList;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getCurr() {
		return curr;
	}

	public void setCurr(String curr) {
		this.curr = curr;
	}

	@Override
	public String toString() {
		return "princes [startDate=" + startDate + ", endDate=" + endDate + ", priceList=" + priceList + ", productId="
				+ productId + ", priority=" + priority + ", price=" + price + ", curr=" + curr + ", brandId=" + brandId
				+ "]";
	}
}
