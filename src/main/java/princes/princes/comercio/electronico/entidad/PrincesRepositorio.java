package princes.princes.comercio.electronico.entidad;

import org.springframework.data.jpa.repository.JpaRepository;


public interface PrincesRepositorio extends JpaRepository<Princes, Integer> {

}
