package princes.princes.comercio.electronico.entidad;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import princes.princes.comercio.electronico.servicio.PrincesDAOServicio;


@RestController
public class PricesJPAResource <Princes>{
	
	@Autowired
	private PrincesDAOServicio princesDaoService;
	
	@Autowired
	private PrincesRepositorio productoRepositorio;

//Extraer todos los productos
@GetMapping("/productos/extraer")
public List<princes.princes.comercio.electronico.entidad.Princes> Productos(){
	return productoRepositorio.findAll();
}

@PostMapping("/productos/asignar")
public String postPersona(@RequestBody Princes prince){
return prince.toString();
}


}
